"""SELECT start_time, 
       Max(fi.rides) AS rides, 
       Max(fi.seats) AS seats 
FROM   (SELECT Time(From_unixtime(d.starttime)) AS start_time, 
               c.rides                          AS rides, 
               e.seats                          AS seats 
        FROM   (SELECT ab.trip_id, 
                       ab.vehicle_number, 
                       Count(ab.booking_id) AS rides 
                FROM   (SELECT a.booking_id, 
                               a.trip_id, 
                               b.vehicle_number 
                        FROM   user_management_systems.bookings AS a 
                               LEFT JOIN 
                               user_management_systems.booking_vehicles AS b 
                                      ON a.booking_id = b.booking_id 
                        WHERE  a.route_id = {0}
                               AND 
                       Date(From_unixtime(a.created_time / 1000)) = '{1}' 
                               AND a.status IN ( "confirmed", "rescheduled" )) 
                       AS ab 
                GROUP  BY 1, 
                          2) AS c 
               LEFT JOIN shuttl.trip AS d 
                      ON c.trip_id = d.id 
               LEFT JOIN shuttl.vehicles AS e 
                      ON c.vehicle_number = e.identifies 
        ORDER  BY 1) fi 
GROUP  BY 1 
"""